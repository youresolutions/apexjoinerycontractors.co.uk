window.onload = init;

function init()
{
	if (!document.getElementsByTagName) return false;
	if (!document.getElementById) return false;
	bkmark();
	prepareForms();
	playflash();
}

function bkmark()
{
	var bookmarks = getElementsByClass('bookmark', null, 'a');
	for (var i = 0; i < bookmarks.length; i++)
	{
		var bookmark = bookmarks[i];
		bookmark.onclick = function()
		{
			// Note: vars bookmarkTitle and bookmarkUrl are global vars which are declared in head.inc.php
			bookmarksite(bookmarkTitle, bookmarkUrl);
		}
	}
	return false;
}

function bookmarksite(title, url)
{
	if (document.all)
	{
		window.external.AddFavorite(url, title);
	}
	else if (window.sidebar)
	{
		window.sidebar.addPanel(title, url, "")
	}
}

function prepareForms()
{
	for (var i = 0; i < document.forms.length; i++)
	{
		var thisForm = document.forms[i];
		autoCompleteOff();
		focusLabels();
		thisForm.onsubmit = function()
		{
			return validateForm(this);
		}
	}
}

// The function below fixes the following Mozilla (older browser versions) bug by turning off autcomplete:
// Error: [Exception... "'Permission denied to get property XULElement.selectedIndex' when calling method: [nsIAutoCompletePopup::selectedIndex]"
function autoCompleteOff()
{
	var inputs = document.getElementsByTagName("input");
	for (i=0; inputs[i]; i++)
	{
		inputs[i].setAttribute("autocomplete", "off");
	}
}

function focusLabels()
{
	var labels = document.getElementsByTagName("label");
	for (var i = 0; i < labels.length; i++)
	{
		if (!labels[i].getAttribute("for")) continue;
		labels[i].onclick = function()
		{
			var id = this.getAttribute("for");
			if (!document.getElementById(id)) return false;
			var element = document.getElementById(id);
			element.focus;
		}
	}
}

function validateForm(whichForm)
{
	var errMsg = "";
	var lineNo = 1;
	for (var i = 0; i < whichForm.elements.length; i++)
	{
		//captcha=document.getElementById("captcha").value;
		
		var element = whichForm.elements[i];
		if (element.className.indexOf("required") != -1)
		{
			if (!isFilled(element) || isDefaultValue(element))
			{
				if (whichForm.id == "recommendform")
				{
					// Recommend form (as in email a friend etc)
					var recommendTo = document.getElementById("recommend_to");
					if (element.name == "to_email")
					{
						if (recommendTo.value != "receipient")
						{
							errMsg += lineNo + ". Your " + recommendTo.value + "'s email address is blank.\n";
						}
						else
						{
							errMsg += lineNo + ". Receipient's email address is blank.\n";
						}
						highlightElement(element);
						lineNo++;
					}
					if (element.name == "to_name")
					{
						if (recommendTo.value != "receipient")
						{
							errMsg += lineNo + ". Your " + recommendTo.value + "'s name is blank.\n";
						}
						else
						{
							errMsg += lineNo + ". Receipient's name is blank.\n";
						}
						highlightElement(element);
						lineNo++;
					}
					if (element.name == "from_name")
					{
						errMsg += lineNo + ". Your name is blank.\n";
						highlightElement(element);
						lineNo++;
					}
					if (element.name == "from_email")
					{
						errMsg += lineNo + ". Your email address is blank.\n";
						highlightElement(element);
						lineNo++;
					}
					
				}
				else
				{
					
					if (element.name == "your_name")
					{
						errMsg += lineNo + ". Your name is blank.\n";
						highlightElement(element);
						lineNo++;
					}
					if (element.name == "your_telephone")
					{
						errMsg += lineNo + ". Your telephone number is blank.\n";
						highlightElement(element);
						lineNo++;
					}
					// Enquiry form

					if (element.name == "name")
					{
						errMsg += lineNo + ". Your name is blank.\n";
						highlightElement(element);
						lineNo++;
					}
					if (element.name == "telephone")
					{
						errMsg += lineNo + ". Your telephone number is blank.\n";
						highlightElement(element);
						lineNo++;
					}
					if (element.name == "email")
					{
						errMsg += lineNo + ". Your email address is blank.\n";
						highlightElement(element);
						lineNo++;
					}
					if (element.name == "enquiry")
					{
						errMsg += lineNo + ". Your enquiry is blank.\n";
						highlightElement(element);
						lineNo++;
					}
					if (element.name == "captcha")
					{
						errMsg += lineNo + ". Your captcha address is blank.\n";
						highlightElement(element);
						lineNo++;
					}
				}
			}
		}
		if (element.className.indexOf("email") != -1)
		{
			if (isFilled(element) && !isEmailAddress(element.value))
			{
				// Recommend form (as in email a friend etc)
				if (whichForm.id == "recommendform")
				{
					var recommendTo = document.getElementById("recommend_to");
					if (element.name == "to_email")
					{
						if (recommendTo.value != "receipient")
						{
							errMsg += lineNo + ". Your " + recommendTo.value + "'s email address is invalid.\n";
						}
						else
						{
							errMsg += lineNo + ". Receipient's email address is invalid.\n";
						}
						highlightElement(element);
						lineNo++;
					}
					if (element.name == "from_email")
					{
						errMsg += lineNo + ". Your email address is invalid.\n";
						highlightElement(element);
						lineNo++;
					}
				}
				else
				{
					// Enquiry form
					if (element.name == "email")
					{
						errMsg += lineNo + ". Your email address is invalid.\n";
						highlightElement(element);
						lineNo++;
					}
				}
			}
		}
	}
	if (errMsg != "")
	{
		showErr(whichForm, errMsg);
		return false;
	}
	else
	{
		return true;
	}
}

function isFilled(elm)
{
	if (elm.value.length < 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}

function isDefaultValue(elm)
{
	if (elm.value == elm.defaultValue)
	{
		return true;
	}
	else
	{
		return false;
	}
}

function isEmailAddress(email)
{
	// Pattern contains all the chars it is okay to use in the local part of an email address
	// For further information visit http://www.remote.org/jochen/mail/info/chars.html
	var pattern = new RegExp(/^([a-zA-Z0-9_\-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/i);
	if (!pattern.test(email))
	{
		return false;
	}
	else
	{
		return true;
	}
}

function getElementsByClass(searchClass, node, tag)
{
	var classElements = new Array();
	if (node == null)
	{
		node = document;
	}
	if (tag == null)
	{
    		tag = '*';
	}
	var els = node.getElementsByTagName(tag);
	var elsLen = els.length;
	var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
	for (i = 0, j = 0; i < elsLen; i++)
	{
    		if (pattern.test(els[i].className) )
		{
      			classElements[j] = els[i];
	      		j++;
		}
	}
	return classElements;
}

function highlightElement(thisElement)
{
	thisElement.style.color = "red";
	thisElement.style.borderColor = "red";
	thisElement.style.borderStyle = "solid";
}

function showErr(thisForm, ErrMsg)
{
	var ErrMsg = "The following errors were found with your submission:\n\n" + ErrMsg;
	alert(ErrMsg);
	return false;
}
function playflash(){
	var fplay=document.getElementById("flash");
    if(!fplay){ return false;}
	else{
    var fo = new FlashObject("flash3.swf", "flashmovie", "300", "141", "7", "high");
	fo.addParam("menu","false");
	fo.addParam("wmode","transparent");

	fo.write(fplay);
	}
}
function largeView(imgName,title, width, height)
{
window.open('largeview.php?img=large/'+imgName+'&title='+title,'','width='+width+',height='+height+',resizable=0,maximize=0,top=50,left=50,menubar=0,toolbar=0');
}