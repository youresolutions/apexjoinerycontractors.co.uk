<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/callback2.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/head.inc.php");
require_once("includes/rightcola.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['gallery'], $ini_array['description']['gallery'], $ini_array['keywords']['gallery'], $ini_array['client_name']['full'], $ini_array['client_name']['full'], $ini_array['web']['short']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
//leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1>Gallery</h1>
<?
$count=1;
$i=1;
$path="thumbnails/";
$opendir=opendir($path);
while($file=readdir($opendir))
{
if($file=="." || $file==".." || $file=="Thumbs.db")
{}
else
{
$imagesize=getimagesize("large/".$file);
$width= $imagesize[0]+50;
$height=$imagesize[1]+80;

?>
<a href="#" onclick="largeView('<?=$file?>','Image <?=$count?>','<?=$width?>','<?=$height?>' )"><img src="<?=$path?>/<?=$file?>" alt="Image <?=$count?>" class="img_gallery" /></a>
<?
$count++;
}
}
?>

</div>
<?php

rightcola($ini_array);
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>