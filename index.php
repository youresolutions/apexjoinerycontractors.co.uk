<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/callback.inc.php");
require_once("includes/rightcola.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['home'], $ini_array['description']['home'], $ini_array['keywords']['home'], $ini_array['client_name']['full'], $ini_array['client_name']['bkmrk'], $ini_array['web']['full']);
?>

<body>	
<div class="wrapper" id="wrapper-a">
<?php
masthead($ini_array);
//leftcol($ini_array);
?>
<div id="middlecol">
<h1>Welcome to <? echo $ini_array['client_name']['full']; ?></h1>
<p>We are a small family run South Yorkshire based business. We have built up a large number of regular customers and businesses in the local and surrounding areas also throughout the U K.</p> 
<div id="callmeback-home">
<? callback($ini_array['client_name']['short'], $ini_array['email']['primary']);?>
</div>
<p><a href="request.php?title=Request a Quote&amp;request=Quote"><img src="images/request-a-quote.jpg" alt="" /></a></p>
<p class="clearBoth">We pride ourselves on our work, no job is too small or too big and we always ensure you are satisfied with our service at all times.</p>
<p>Check back soon for the new website, and bookmark this page to keep up-to-date with all new developments from <strong><?=$ini_array['client_name']['full']?>.</strong></p>
</div>
<?php
rightcola($ini_array);
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>