<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/callback2.inc.php");
require_once("includes/rightcola.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/callback.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
/* Document head */
head($ini_array['title']['portfolio'], $ini_array['description']['portfolio'], $ini_array['keywords']['portfolio'], $ini_array['client_name']['full'], $ini_array['client_name']['full'], $ini_array['web']['short']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
//leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1>Portfolio</h1>
<p>A selection of the recent sites that we have developed for clients in Doncaster.</p>
<ul>
<li><a href="http://www.barnets.co.uk">Barnets Hair Studio</a></li>
<li><a href="http://www.beautifulselling.com">Beautiful Selling</a></li>
<li><a href="http://www.blaxtonboats.com">Blaxton Boats</a></li>
<li><a href="http://www.clairewhite.co.uk">Claire White IT Training</a></li>
<li><a href="http://www.company-formations-registrations.co.uk">Company Formations</a></li>
<li><a href="http://www.ddt-dsd.org.uk">Doncaster School for the Deaf</a></li>
<li><a href="http://www.deaf-college.com">Doncaster College for the Deaf</a></li>
<li><a href="http://www.ecdgassparestoday.co.uk">ECD Gas Spares Today</a></li>
<li><a href="http://www.first-business-systems.co.uk">First Business Systems</a></li>
<li><a href="http://www.fisheyestudio.co.uk">Fisheye Photography Studio</a></li>
<li><a href="http://www.garrythickett.co.uk">Garry Thickett - Chartered Management Accountant</a></li>
<li><a href="http://www.girleshop.co.uk">Girl eShop</a></li>
<li><a href="http://www.keeptheheaton.com">Keep The Heat On Gas &amp; Heating Spares</a></li>
<li><a href="http://www.lambertwelding.co.uk">Lambert Welding</a></li>
<li><a href="http://www.pink-orchid.com">Pink Orchid - Doncaster Health &amp; Beauty Salon</a></li>
<li><a href="http://www.premier-calibration.co.uk">Premier Calibrations</a></li>
<li><a href="http://www.plantuk.com">Plant UK</a></li>
</ul>
</div>
<?php
rightcola($ini_array);
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>