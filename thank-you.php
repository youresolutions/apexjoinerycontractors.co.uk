<?php
/* Parse ini files and merge them */
$client_array = parse_ini_file('config/client.ini.php', true);
$site_array = parse_ini_file('config/site.ini.php', true);
$ini_array = array_merge($client_array, $site_array);
/* Includes */
require_once("includes/head.inc.php");
require_once("includes/callback2.inc.php");
require_once("includes/rightcola.inc.php");
require_once("includes/masthead.inc.php");
require_once("includes/leftcol.inc.php");
require_once("includes/footer.inc.php");
require_once("includes/google-analytics-code.inc.php");
require_once("includes/callback.inc.php");
/* Document head */
head($ini_array['title']['thank_you'], $ini_array['description']['thank_you'], $ini_array['keywords']['thank_you'], $ini_array['client_name']['full'], $ini_array['client_name']['full'], $ini_array['web']['short']);
?>

<body>
<div class="wrapper" id="wrapper-b">
<?php
masthead($ini_array);
//leftcol($ini_array);
?>
<div class="rightcol" id="rightcol-b">
<h1>Thank You</h1>
<p>Thank you for your enquiry!</p>
<p>We will be in contact with you promptly.</p>
</div>
<?php
rightcola($ini_array);
footer($ini_array['client_name']['footer']);
?>
</div>
<?php
google_analytics_code($ini_array['google']['uacct']);
?>
</body>
</html>