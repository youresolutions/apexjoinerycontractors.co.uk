<?php

function footer($full_client_name)
{
	echo "<div id=\"footer\">\n";
	echo "<p>&copy;".date(Y)," ".$full_client_name." All rights reserved. <a href=\"http://www.yesl.co.uk\">Web design</a> by Your e Solutions Ltd.</p>\n";
	echo "</div>\n";
}

?>