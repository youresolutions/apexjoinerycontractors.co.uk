<?php

function callback2($client_name, $email_address)
{

	if (!isset($_POST['your_name']) || !isset($_POST['your_telephone']))
	{
		echo<<<EOF
EOF;
echo "\n<form id=\"callbackform\" action=\"".$_SERVER['PHP_SELF']."\" method=\"post\">\n";
echo<<<EOF
<p><label class='one'>Your name:</label><input type="text" class="required one" id="yourname" name="your_name" maxlength="60" tabindex="100" /></p>

<p><label class='one'>Your telephone:</label>
<input style="float:left;" type="text" class="required two" id="yourtelephone" name="your_telephone" maxlength="20" tabindex="101" />
<input style="margin-left:2px;" type="submit" class="three" name="submit" value="Call Me" tabindex="102" /></p>
</form>
EOF;
	}
	else
	{
		$to = "priya.sharma@dotsquares.com";
		$subject = "Call Back Request for Priya";
		$body = $_POST['your_name']." would like you to call back on: ".$_POST['your_telephone'];
		$header = "From: ".$email_address;
		if (@mail($to, $subject, $body, $header))
		{
			echo "<p style='font-size:10px; margin-left:10px; margin-top:10px; margin-right:30px; clear:both;'>We have recieved your request and will be contacting you shortly.</p>";
		}
		else
		{
			echo "<p style='font-size:10px; margin-right:40px; clear:both;'>Sorry, we were unable to send your request. Please use one of the other ways to contact us.</p>";
		}
	}
}

?>