<?php

function callback($client_name, $email_address)
{
	if (!isset($_POST['your_name']) || !isset($_POST['your_telephone']))
	{
		echo<<<EOF
<h2>Callback Free</h2>
EOF;
echo "\n<form id=\"callbackform\" action=\"".$_SERVER['PHP_SELF']."\" method=\"post\">\n";
echo<<<EOF
<p><label for="yourname">Your name:</label><input type="text" class="required" id="yourname" name="your_name" maxlength="60" tabindex="100" /><span class="warning">*</span></p>
<p><label for="yourtelephone">Your telephone:</label><input type="text" class="required" id="yourtelephone" name="your_telephone" maxlength="20" tabindex="101" /><span class="warning">*</span></p>
<p><input class='submit' type="submit"  name="submit" value="Call Me Back" tabindex="102" /></p>
</form>
<p class='clearBoth'>Fields marked with an asterisk (<span class="warning">*</span>) must be completed.</p>
EOF;
	}
	else
	{
		$to = $email_address;
		$subject = "Call Back Request for ".$client_name;
		$body = $_POST['your_name']." would like you to call back on: ".$_POST['your_telephone'];
		$header = "From: " . $to;
		if (@mail($to, $subject, $body, $header))
		{
			echo "<p class=\"emphasis\">We have recieved your request and will be contacting you shortly.</p>";
		}
		else
		{
			echo "<p class=\"error\"><b>Sorry, we were unable to send your request. Please use one of the other ways to contact us.</b></p>";
		}
	}
}

?>