<?php

function masthead($ini_array)
{
	echo "<div id=\"masthead\">\n";
	echo "<span id=\"slogan\">".$ini_array['phrase']['slogan']."</span>\n";
	echo "</div>\n";
	echo "<div id=\"topnav\">\n";
	echo "<ul>\n";
	echo "<li><a href=\"index.php\" title=\"".$ini_array['link_title']['home']."\">Home</a></li>\n";
	echo "<li><a href=\"about-us.php\" title=\"".$ini_array['link_title']['about_us']."\">About Us</a></li>\n";
	echo "<li><a href=\"gallery.php\" title=\"Gallery\">Gallery</a></li>\n";
	echo "<li><a href=\"enquiry-form.php\" title=\"".$ini_array['link_title']['enquiry_form']."\">Enquiry Form</a></li>\n";
	echo "<li class=\"lastlink\"><a href=\"contact-us.php\" title=\"".$ini_array['link_title']['contact_us']."\">Contact Us</a></li>\n";
	echo "</ul>
	\n";
	if (!empty($ini_array['phone_number']['call_now']))
	{
		
		echo "<div id=\"callnow\">".$ini_array['phone_number']['call_now']."</div>\n";
		echo "<div id=\"call\"></div>";
	}
	
	echo "</div>\n";
	
	
}
?>