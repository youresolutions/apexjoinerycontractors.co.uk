<?php

function head($title, $description, $keywords, $author, $bookmark_title, $bookmark_url)
{
	if (empty($title) || empty($description) || empty($keywords))
	{
		die("One or more is missing: title, meta keywords or meta description.");
	}
	echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
	echo "<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\" lang=\"en\">\n";
	echo "<head>\n";
	echo "<title>".$title."</title>\n";
	echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\" />\n";
	echo "<meta name=\"description\" content=\"".$description."\" />\n";
	echo "<meta name=\"keywords\" content=\"".strtolower($keywords)."\" />\n";
	echo "<meta name=\"author\" content=\"".$author."\" />\n";
	echo "<meta http-equiv=\"Content-Style-Type\" content=\"text/css\" />\n";
	echo "<style type=\"text/css\">\n";
	echo "@import url(\"css/master.css\");\n";
	echo "</style>\n";
	echo "<script type=\"text/javascript\">var bookmarkTitle=\"".$bookmark_title."\"; var bookmarkUrl=\"".$bookmark_url."\";</script>\n";
	echo "<script type=\"text/javascript\" src=\"js/master.js\"></script>\n";
	echo "<script type=\"text/javascript\" src=\"js/flashobject.js\"></script>\n";
		if($title=="How To Find Us")
	{
	echo<<<eof
	<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=ABQIAAAAh3CSaRC72cS7sZvGQBonPhQnwJdfFH5fIQFT4LUtzh1eyTQI7xSSwGzdJCNOg6-7W1dCrpbgWkKRwQ"
      type="text/javascript"></script>


<script type="text/javascript">	  
function googleMap()
{
		var map= new GMap2(document.getElementById('gmap'));
		if(!gmap)
		{return;}
		map.setCenter(new GLatLng(53.35007, -1.33562), 13);
	    var point = new GLatLng(53.35007, -1.33562);
		map.addOverlay(new GMarker(point));
		addLargeMapControls(map);
}
function addLargeMapControls(map)
{
	map.addControl(new GLargeMapControl());
	map.addControl(new GMapTypeControl());
	map.addControl(new GOverviewMapControl());
	//map.addControl(new GScaleControl());
	map.enableDoubleClickZoom();
}
</script>
<link rel="stylesheet" href="css/print.css" media="print" />
eof;
}
	echo "</head>\n";
}

?>