<?php

[client_name]
contact="Apex Joinery Contractors";
footer="apexjoinerycontractors.co.uk. "
bkmrk= "apexjoinerycontractors.co.uk" ;
full = "Apex Joinery Contractors" ; required
short = "apexjoinerycontractors.co.uk" ; required - if not a limited company, make the same as the full name above
abbrev = "apexjoinerycontractors.co.uk" ; optional

[contact_person]
; First two lines of postal address on Contact Us page
name = "" ; optional
job_title = "" ; optional

[office]
office_1="";
office_2="";

[address]
line_1 = "5 Ryton Road" ; optional
line_2 = "Sheffield" ; optional
line_3 = "South Yorkshire"
postcode = "S25 4DL" ; optional

[reg_office]
line_1 = "" ; optional
line_2 = "" ; optional
line_3 = "" ; optional
postcode = "" ; optional
reg_no= "";

[phone_number]
call_now = "Call Now: 01909 774573" ; optional
telephone_1 = "01909 774573" ; optional
telephone_2 = "" ; optional
mobile_1 = "" ; optional
mobile_2 = "" ; optional
fax_1 = "" ; optional
fax_2 = "" ; optional

[email]
primary = "info@apexjoinerycontractors.co.uk" ; required
other = "" ; optional
notification = "automailer@yesl.co.uk" ; required

[web]
redirect="http://www.apexjoinerycontractors.co.uk";
sites="http://www.apexjoinerycontractors.co.uk";
full = "http://www.apexjoinerycontractors.co.uk" ; required e.g. http://www.yesl.co.uk
short = "www.apexjoinerycontractors.co.uk" ; required e.g. www.yesl.co.uk

[phrase]
slogan = "" ; optional
headline = "" ; optional
strapline = "" ; optional
tagline = "" ; optional
catch = "" ; optional
motto = "" ; optional

?>
