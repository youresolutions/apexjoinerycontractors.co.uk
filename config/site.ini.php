<?php

; IMPORTANT: EACH PAGE SHOULD HAVE ITS OWN TITLE, DESCRIPTION AND LIST OF KEYWORDS

[title]
services= "Apex Joinery Contractors - Services"
about_us= "Apex Joinery Contractors - About Us"
products= "Apex Joinery Contractors - Products"
gallery = "Apex Joinery Contractors - Image Gallery"
home = "Apex Joinery Contractors - Home"
call_me_back = "Apex Joinery Contractors - Call me back"
contact_us = "Apex Joinery Contractors - Ways to contact to us"
enquiry_form = "Apex Joinery Contractors - Enquiry form"
recommend = "Apex Joinery Contractors - Recommend this site"
thank_you = "Thank You"

[description]
services= "Apex Joinery Contractors - Services"
about_us= "Apex Joinery Contractors - About Us"
products= "Apex Joinery Contractors - Products"
gallery = "Apex Joinery Contractors - Image Gallery"
home = "Apex Joinery Contractors"
call_me_back = "Call me back"
contact_us = "How to contact us."
enquiry_form = "You can use our Enquiry form to request any information that you cannot find on this site."
recommend = "Recommend this site to a friend, colleague or someone you know."
thank_you = "Your enquiry is important to us. We will be in contact with you promptly."

[keywords]
services= "Apex Joinery Contractors Services"
about_us= "Apex Joinery Contractors About Us"
products= "Apex Joinery Contractors Products"
gallery = "Apex Joinery Contractors Image Gallery"
home = "Apex Joinery Contractors home"
call_me_back = "callback call me back phone"
contact_us = "contact callback call back letter write address telephone mobile fax email site website webmaster car location map"
enquiry_form = "enquiry form request information site website"
recommend = "recommend tell email friend colleague site website"
thank_you = "thank you enquiry promptly shortly soon possible"

[link_title]
home = "Homepage"
enquiry_form = "Request information you cannot find on this site"
contact_us = "Different ways to contact us"
recommend_friend = "Recommend this site to a friend"
recommend_colleague = "Recommend this site to a colleague"
recommend_receipient = "Recommend this site to someone you know"
bookmark = "Add this site to your favorites/bookmarks"
callback = "Request a callback"
multimap = "Go to Multimap zoomable map showing our location"

[google]
uacct = "UA-6817826-1" ; Analytics User Account string (it will be something like UA-12345-6)
?>